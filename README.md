## Instalacion
- Clonen el repositorio o bajenlo en zip
- Ejecuten `composer install`
- Ejecuten `npm install && npm run dev`
- Realicen la migracion, y `php artisan db:seed` para llenar con productos
- Pongan sus claves de stripe en el archivo .env
